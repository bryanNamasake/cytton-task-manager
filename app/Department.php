<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    public function employee(){
        return $this->belongsTo('App\Employee');

    }

    public function user(){
        return $this->hasOne('App\Employee');

    }

    public function departmentTask() {
        return $this->hasMany('App\DepartmentTask');
    }
}
