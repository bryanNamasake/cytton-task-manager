<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DepartmentNotifications extends Model
{
    protected $fillable = [
        'department_id', 'notification_title', 'notification_body',
        'redirect_link', 'notification_check'
    ];
}
