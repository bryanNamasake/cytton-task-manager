<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DepartmentTask extends Model
{
    protected $fillable = [
        'task_id', 'department_id'
    ];

    public function task() {
        return $this->belongsTo('App\Task');
    }

    public function Department() {
        return $this->belongsTo('App\Department');
    }
}
