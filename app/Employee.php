<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'employee_id', 'avatar'
    ];

    public function department(){
        return $this->belongsTo('App\Department');

    }

    public function user(){
        return $this->hasOne('App\User', "employee_id");

    }
}
