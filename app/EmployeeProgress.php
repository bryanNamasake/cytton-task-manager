<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeProgress extends Model
{
    protected $table = "employee_progress";

    protected $fillable = [
        'user_id', 'progress', 'progress_message'
    ];

    public function user() {
        return $this->belongsTo('App\User');
    }
}
