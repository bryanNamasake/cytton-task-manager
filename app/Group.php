<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    protected $fillable = [
        'name', 'user_id', 'task_id'
    ];

    public function user() {
        return $this->belongsTo('App\User');
    }

    public function groupTask() {
        return $this->hasMany('App\GroupTask');
    }

    public function employeeGroup(){
        return $this->hasMany('App\EmployeeGroup');
    }

    /*public function task(){
        return $this->hasMany('App\Task');
    }*/
}
