<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GroupNotifications extends Model
{
    protected $fillable = [
        'group_id', 'notification_title', 'notification_body',
        'redirect_link', 'notification_check'
    ];
}
