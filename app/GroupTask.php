<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GroupTask extends Model
{
    protected $fillable = [
        'task_id', 'group_id'
    ];


    public function task() {
        return $this->belongsTo('App\Task');
    }

    public function group() {
        return $this->belongsTo('App\Group');
    }
}
