<?php
/**
 * Created by PhpStorm.
 * User: MACHINE
 * Date: 1/29/2018
 * Time: 10:17 AM
 */

namespace App\Http\Controllers;


class BaseApiController extends Controller
{
    private $responseStatus = [
        'status' => [
            'isSuccess' => true,
            'statusCode' => 200,
            'message' => '',
        ]
    ];

    // Setter method for the response status
    public function setResponseStatus(bool $isSuccess = true, int $statusCode = 200, string $message = '')
    {
        $this->responseStatus['status']['isSuccess'] = $isSuccess;
        $this->responseStatus['status']['statusCode'] = $statusCode;
        $this->responseStatus['status']['message'] = $message;
    }

    // Returns the response with only status key
    public function sendResponseStatus($isSuccess = true, $statusCode = 200, $message = '')
    {

        $this->responseStatus['status']['isSuccess'] = $isSuccess;
        $this->responseStatus['status']['statusCode'] = $statusCode;
        $this->responseStatus['status']['message'] = $message;

        $json = $this->responseStatus;

        return response()->json($json, $this->responseStatus['status']['statusCode']);

    }

    // If you have additional data to send in the response
    public function sendResponseData($data)
    {

        if(!empty($this->meta)) $data['meta'] = $this->meta;

        $json = [
            'status' => $this->responseStatus['status'],
            'data' => $data,
        ];


        return response()->json($json, $this->responseStatus['status']['statusCode']);

    }


}