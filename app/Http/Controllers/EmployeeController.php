<?php

namespace App\Http\Controllers;

use App\Employee;
use App\Http\Resources\Employee as EmployeeResource;
use App\User;
use Illuminate\Http\Request;

class EmployeeController extends BaseApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $employees = Employee::paginate(10);
        return EmployeeResource::collection($employees);
    }


    public function verifyEmployee(Request $request){

        $employee = Employee::where("email" , $request->email_address)->first();

        if(!$employee) {
            return $this->sendResponseStatus(false, 401, 'User does not exist');
        }
        $this->setResponseStatus(true, 200, 'User Exists');
        return $this->sendResponseData($employee);
    }


    public function get_department(Request $request){
        $employee = Employee::where("employee_id" , $request->employee_id)->first();

        if(!$employee) {
            return $this->sendResponseStatus(false, 401, 'User does not exist');
        }
        $this->setResponseStatus(true, 200, 'User Exists');
        return $this->sendResponseData($employee);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $matched_results = Employee::search($request->input('search_value'));
        return response()->json($matched_results);
    }

    public function search(Request $request)
    {
        $searchTerm = $request->input('search_value');
        $matched_results = User::where('employee_id', 'like', '%' .$searchTerm. '%')
                ->orWhere('email', 'like', '%' .$searchTerm. '%')
                ->orWhere('name', 'like', '%' .$searchTerm. '%')->get();
        return response()->json($matched_results);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $employee = Employee::findOrFail($id);
        return new EMployeeResource($employee);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function edit(Employee $employee)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Employee $employee)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function destroy(Employee $employee)
    {
        //
    }
}
