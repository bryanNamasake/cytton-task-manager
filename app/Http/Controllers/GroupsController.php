<?php

namespace App\Http\Controllers;

use App\EmployeeGroup;
use App\Group;
use App\Providers\AuthServiceProvider;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;

class GroupsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $uid = Auth::user()->id;

        //dd(User::find(16)->employeeGroup->first()->group->user->name);
        return view("task-app.team.index")
            ->with('employeeGroups', User::find(16)->employeeGroup);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("task-app.team.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user_ids = $request->user_id;
        $current_user = Auth::user()->id;
        $this->validate($request, [
            'name' => 'required|max:255',
        ]);

        $group = Group::create([
            'name' => $request->name,
            'user_id' => $current_user
        ]);

        $user_ids[] = $current_user;

        foreach($user_ids as $user_id) {
            EmployeeGroup::create([
                'user_id' => $user_id,
                'group_id' => $group->id
            ]);
        }
        Session::flash('success', 'Group Created Successfully Created Successfully');

        return redirect()->route('app.home');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $group = Group::where('id', $id)->first();

        return view("task-app.team.view")
            ->with('group_details', $group);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
