<?php

namespace App\Http\Controllers;

use App\Employee;
use App\User;
use function foo\func;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Facades\Auth;
use Session;
use SocialAuth;
use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use SocialNorm\Exceptions\ApplicationRejectedException;
use SocialNorm\Exceptions\InvalidAuthorizationCodeException;

class SocialsController extends Controller
{
    public function auth($provider){
        //
        return SocialAuth::authorize($provider);
    }

    public function auth_callback($provider){
        //dd($provider);
        try {
            SocialAuth::login($provider, function ($user, $details) {
                //dd($details);
                $client = new Client(); //GuzzleHttp\Client
                try {
                    $api_response = $client->post('https://elimusmart.com/task-management/public/api/v1/employee/verify', [
                        'form_params' => [
                            'email_address' => $details->email
                        ]
                    ]);

                    $response = json_decode($api_response->getBody()->getContents());

                    if ($response->status->isSuccess) {

                        $employee = $response->data;
                        if (!User::where('employee_id', $employee->employee_id)->first()) {
                            $user = new User();
                            $user->avatar = $details->avatar;
                            $user->name = $details->full_name;
                            $user->email = $details->email;
                            $user->employee_id = $employee->employee_id;
                            $user->notification_check_time = date("");
                            //dd($user->email);
                            $user->save();
                        }

                        session(['employee_id' => $employee->employee_id]);
                    } else {
                        Session::flash('error', "user does not exist");
                    }
                } catch (ClientException $e) {
                    Session::flash('error', $e->getMessage());
                }
            });
        }catch (ApplicationRejectedException $e) {
            // User rejected application
            dd('error1');
        } catch (InvalidAuthorizationCodeException $e) {
            // Authorization was attempted with invalid
            // code,likely forgery attempt
            print_r($provider);
            dd($e);
        }
        return redirect()->route('app.home');
    }
}
