<?php

namespace App\Http\Controllers;

use App\DepartmentNotifications;
use App\DepartmentTask;
use App\EmployeeProgress;
use App\Group;
use App\GroupNotifications;
use App\GroupTask;
use App\Http\Resources\Department;
use App\Department as DepartmentModel;
use App\IndividualTask;
use App\Notification;
use App\Task;
use App\User;
use App\TaskAttachment;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;
use SocialNorm\Exceptions\ApplicationRejectedException;
use SocialNorm\Exceptions\InvalidAuthorizationCodeException;

class TasksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $uid = Auth::user()->id;

        //dd(User::find(16)->individualTask->first()->task->priority);
        //dd(User::with('Task','IndividualTask')->find(14));

        try {
        $client = new Client(); //GuzzleHttp\Client
        $api_response = $client->post('https://elimusmart.com/task-management/public/api/v1/employee/get_department', [
            'form_params' => [
                'employee_id' => Auth::user()->employee_id
            ]
        ]);

        $response = json_decode($api_response->getBody()->getContents());
        $employee_dep = "";
        if ($response->status->isSuccess) {
            $employee_dep = $response->data->department_id;
        }

        //dd($employee);
        }catch (ApplicationRejectedException $e) {
            // User rejected application
            dd('error1');
        } catch (InvalidAuthorizationCodeException $e) {
            // Authorization was attempted with invalid
            // code,likely forgery attempt
            dd($e);
        }

        $tasks = User::find($uid)->individualTask;
        $groupTasks = User::find($uid)->group;
        /*$departmentTasks = DepartmentTask::where('department_id', $employee_dep)->get();

        dd($departmentTasks->task);*/
        return view("task-app.task.index")
            ->with('groupTask', $groupTasks)
            ->with('individualTask', $tasks);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $departments = DepartmentModel::all();
        $groups = Group::all();

        return view("task-app.task.create")
            ->with('departments', $departments)
            ->with('groups', $groups);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user_ids = $request->user_id;
        $current_user = Auth::user()->id;
        $has_attachment = "no";
        $file_new_name = "";

        $this->validate($request, [
            'name' => 'required|max:255',
            'description' => 'required',
            'priority_picker' => 'required',
            'start_date' => 'required',
            'end_date' => 'required',
            'assign_category' => 'required',
            'make_public' => 'required'
        ]);

        if($request->hasFile('file_attachment'))
        {
            $has_attachment = "yes";
        }

        $task = Task::create([
            'title' => $request->name,
            'user_id' => $current_user,
            'description' => $request->description,
            'priority' => $request->priority_picker,
            'attachment' => $has_attachment,
            'assigned_by' => $request->assign_category,
            'completed' => "no",
            'reassigned' => "no",
            'start_date' => $request->start_date,
            'end_date' => $request->end_date,
        ]);

        if($has_attachment == "yes"){
            $files = $request->file('file_attachment');
            foreach ($files as $file) {
                $file_new_name = time(). $file->getClientOriginalName();
                $file->move('file_attachments', $file_new_name);
                TaskAttachment::create([
                    'task_id' => $task->id,
                    'attachment_url' => 'file_attachments/'. $file_new_name
                ]);
            }


        }

        switch ($request->assign_category){
            case "group":{
                foreach ($request->groups as $group_id){
                    GroupTask::create([
                        'task_id' => $task->id,
                        'group_id' => $group_id
                    ]);
                    GroupNotifications::create([
                        "group_id" => $group_id,
                        "notification_title" => $request->name. "created ",
                        "notification_body" => "You have been invited to work on this task. This is an individual task.",
                        "redirect_link" => "task/".$task->id,
                        "notification_check" => "no",
                    ]);
                }

                break;
            }
            case "individual":{
                $users = $request->user_id;
                $users[] = Auth::user()->id;
                foreach ($users as $user_id){
                    IndividualTask::create([
                        'task_id' => $task->id,
                        'user_id' => $user_id
                    ]);
                    if($user_id != Auth::user()->id){
                        Notification::create([
                            "user_id" => $user_id,
                            "notification_title" => $request->name. "created ",
                            "notification_body" => "You have been invited to work on this task. This is an individual task.",
                            "redirect_link" => "task/".$task->id,
                            "notification_check" => "no",
                        ]);
                    }
                }

                break;
            }
            case "department":{
                foreach ($request->departments as $department){
                    DepartmentTask::create([
                        'task_id' => $task->id,
                        'department_id' => $department
                    ]);

                    DepartmentNotifications::create([
                        "department_id" => $department,
                        "notification_title" => $request->name. "created ",
                        "notification_body" => "You have been invited to work on this task. This is an individual task.",
                        "redirect_link" => "task/".$task->id,
                        "notification_check" => "no",
                    ]);
                }
                break;
            }
            default:{
                break;
            }
        }


        Session::flash('success', 'Task Created Successfully Created Successfully');

        return redirect()->route('app.home');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $task = Task::where('id', $id)->first();
        /*$taskParticipants = IndividualTask::where('task_id', $id);
        dd($taskParticipants);*/
        return view("task-app.task.view")
            ->with('task_details', $task);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }


    public function progress(Request $request)
    {


        /*$this->validate($request, [
            'progress_picker' => 'progress',
            'description' => 'required'
        ]);*/

        $uid = Auth::user()->id;

        $progress = User::find($uid)->employeeProgress;

        if($progress == null){
            $progress = new EmployeeProgress();
            $progress->user_id = $uid;
        }else{
            if($progress->progress > $request->progress_picker){

                Session::flash('info', 'You cant update your progress negatively ');

                return redirect()->back();
            }
        }

        $progress->progress = $request->progress_picker;
        $progress->progress_message = $request->description;

        $progress->save();

        Session::flash('success', 'Your progress Has been updated Successfully');

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
