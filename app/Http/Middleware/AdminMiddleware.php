<?php

namespace App\Http\Middleware;
use App\Employee;
use Illuminate\Support\Facades\Auth;
use Session;

use Closure;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Session::has('employee_id')){
            if($employee = Employee::where("employee_id" , $request->session()->get('employee_id'))->first()){
                return $next($request);
            }
            return view('home');
        }elseif ( Auth::check()){
            return $next($request);
        }else{
            return redirect()->route('home');
        }

    }
}
