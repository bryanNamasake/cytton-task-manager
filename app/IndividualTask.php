<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IndividualTask extends Model
{
    protected $table = "individual_tasks";
    protected $fillable = [
        'task_id', 'user_id'
    ];

    public function user() {
        return $this->belongsTo('App\User');
    }

    public function task() {
        return $this->belongsTo('App\Task');
    }

}
