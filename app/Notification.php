<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    protected $fillable = [
        'user_id', 'notification_title', 'notification_body',
        'redirect_link', 'notification_check'
    ];
}
