<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $fillable = [
        'title', 'user_id', 'description', 'priority',
        'attachment', 'assigned_by', 'completed',
        'reassigned', 'start_date', 'end_date'
    ];


    public function individualTask() {
        return $this->hasMany('App\IndividualTask');
    }

    public function groupTask() {
        return $this->hasMany('App\GroupTask');
    }

    public function departmentTask() {
        return $this->hasMany('App\DepartmentTask');
    }

    public function user() {
        return $this->belongsTo('App\User');
    }

    public function tackAttachment() {
        return $this->hasMany('App\TaskAttachment');
    }

    public function taskUser() {
        return $this->hasMany('App\User');
    }

    /*public function group() {
        return $this->belongsTo('App\Group');
    }*/
}
