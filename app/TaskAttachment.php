<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TaskAttachment extends Model
{
    protected $fillable = [
        'task_id', 'attachment_url'
    ];

    public function task() {
        return $this->belongsTo('App\Task');
    }


}
