<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'employee_id', 'avatar', 'email'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public function individualTask() {
        return $this->hasMany('App\IndividualTask');
    }

    public function task(){
        return $this->hasMany('App\Task');
    }

    public function employeeProgress() {
        return $this->hasOne('App\EmployeeProgress');
    }

    public function group(){
        return $this->hasMany('App\Group');
    }

    public function employee(){
        return $this->belongsTo('App\Employee', "employee_id");
    }

    public function Department(){
        return $this->belongsTo('App\Department');
    }

    public function employeeGroup(){
        return $this->hasMany('App\EmployeeGroup');
    }
}
