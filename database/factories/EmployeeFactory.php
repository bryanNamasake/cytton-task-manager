<?php

use Faker\Generator as Faker;

$factory->define(App\Employee::class, function (Faker $faker) {
    return [
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'email' => $faker->email,
        'id_number' => $faker->numberBetween(20000000, 40000000),
        'phone_number' => $faker->numberBetween(700000000, 799999999),
        'employee_id' => $faker->numberBetween(10000, 100000),
        'job_level' => $faker->randomElement(['supervisor', 'junior']),
        'department_id' => $faker->numberBetween(1,10),
        'created_at' => $faker->dateTimeBetween('-10 years', 'now'),
        'department_admin' => $faker->boolean
    ];
});
