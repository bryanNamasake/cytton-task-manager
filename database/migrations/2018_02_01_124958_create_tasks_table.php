<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->integer('user_id');
            $table->text('description');
            $table->integer('priority');
            $table->enum('attachment',["yes", "no"]);
            $table->enum('assigned_by', ["group", "individual", "department"]);
            $table->enum("completed", ["yes", "no"]);
            $table->enum("reassigned", ["yes", "no"]);
            $table->timestamp("start_date");
            $table->timestamp('end_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tasks');
    }
}
