<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDepartmentNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('department_notifications', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('department_id');
            $table->string('notification_title');
            $table->string('notification_body');
            $table->string('redirect_link');
            $table->enum('notification_check', ["yes", "no"]);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('department_notifications');
    }
}
