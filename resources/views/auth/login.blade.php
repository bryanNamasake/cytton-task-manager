<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>TASK MANAGEMENT</title>

    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <link href="{{ asset('css/theme/theme.css') }}" rel="stylesheet">
    <link href="{{ asset('plugins/bootstrap/bootstrap.min.css') }}" rel="stylesheet">

    <!-- Styles -->
</head>
<body>
<div class="flex-center position-ref full-height">
    <div class="app-wrapper">
        <header class="main-header">
            <div class="navigation">
                <div class="logo">
                    <a href="{{route('home')}}"><img src="{{ asset('images/cytonn_logo.svg') }}"></a>
                </div>
            </div>
        </header>
        <div class="section-container">
            <div class="container-login">
                <h2>Login and work</h2>

                <hr class="separator">
                <div class="social-login">
                    <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <div class="col-md-12 text-center">
                                <input id="email" type="email" placeholder="Email address"
                                       class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <div class="col-sm-12">
                                <input id="password" type="password" placeholder="Password"
                                       class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-12 text-center">
                                <button type="submit" class="btn btn-outline-success">
                                    Login
                                </button>
                            </div>
                        </div>
                    </form>

                    <hr class="separator">
                    <p class="">If you have no account please login via google</p>
                    <a href="{{ route('social.auth', ['provider' => 'google']) }}" class="btn btn-block btn-outline-danger">google login</a>
                </div>
                <hr class="separator">
                <div class="login-footer">
                    <p class="text-muted">Driving better work management</p>
                </div>
            </div>
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" ></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
</div>
</body>
</html>

