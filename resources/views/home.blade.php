<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>TASK MANAGEMENT</title>

        <meta name="csrf-token" content="{{ csrf_token() }}">
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
        <link href="{{ asset('css/theme/theme.css') }}" rel="stylesheet">

        <!-- Styles -->
    </head>
<body>
<div class="flex-center position-ref full-height">
    <div class="app-wrapper">
    <header class="main-header">
        <div class="navigation">
            <div class="logo">
                <a href="{{route('home')}}"><img src="{{ asset('images/cytonn_logo.svg') }}"></a>
            </div>
        </div>
    </header>
     <div class="section-container">
        <div class="container-login">
            <h2>Login and work</h2>
            <hr class="separator">
            <div class="social-login">
                <p class="pb-3">Use your google account to login</p>
                <a href="{{ route('social.auth', ['provider' => 'google']) }}" class="btn btn-outline-danger">google login</a>
            </div>
            <hr class="separator">
            <div class="login-footer">
                <p class="text-muted">Driving better work management</p>
            </div>
        </div>
     </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" ></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
</div>
</body>
</html>

