<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>TASK MANAGEMENT</title>

    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Fonts -->
    <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <link href="{{ asset('plugins/bootstrap/bootstrap.min.css') }}" rel="stylesheet">
    @yield('styles')
    <link href="{{ asset('css/theme/theme.css') }}" rel="stylesheet">
    <link href="{{ asset('plugins/toastr/toastr.min.css') }}" rel="stylesheet">
    <!-- Styles -->
</head>
<body>
<div class="flex-center position-ref full-height">
    <div class="app-wrapper app-main">
        <div class="common-header">
            <div class="navigation">
                <div class="logo">
                    <a href="{{route('home')}}"><img src="{{ asset('images/cytonn_logo.svg') }}"></a>
                </div>

                <div class="search-box-holder">
                    <div class="search-box">
                        <div class="search-icon"><img src="{{asset('images/search.png')}}" width="20"></div>

                        <form action="" method="post">
                            {{csrf_field()}}
                            <div class="form-group">
                                <input type="text"  id="tag"
                                       name="tag" class="form-control search-input"
                                       placeholder="search tasks, follow-ups, groups">
                            </div>
                        </form>
                    </div>
                </div>

                <div class="profile-holder">
                    <div class="profile">
                        <div class="profile-image">
                            <a href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                                         document.getElementById('logout-form').submit();">
                                Logout
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </div>
                    </div>
                </div>
                <div class="create-task">
                    <a href="{{route("task.create")}}" class="btn btn-success">New Task</a>
                </div>

            </div>
        </div>
        <div class="container">
            <div class="main-navigation">
                <nav class="left-bar-navigation">
                    <ul class="left-bar-navigation__main">
                        <li><a href="{{route("app.home")}}"><i class="fas fa-tachometer-alt fa-lg"></i> <span style="padding-left: 5px">Dashboard</span></a> </li>
                        <li><a href="{{route("app.tasks")}}"><i class="fab fa-elementor fa-lg"></i> <span style="padding-left: 12px">Tasks</span></a> </li>
                        <li><a href="{{route("app.notifications")}}"><i class="far fa-bell fa-lg"></i> <span style="padding-left: 13px">Notifications</span></a> </li>
                        <li><a href="{{route("app.groups")}}"><i class="fas fa-users fa-lg"></i> <span style="padding-left: 6px">Teams</span></a> </li>
                        <li><a href="{{route("app.messages")}}"><i class="far fa-envelope fa-lg"></i> <span style="padding-left: 11px">Messages</span></a> </li>
                    </ul>
                </nav>
                <hr class="separator">
                <nav class="left-bar-navigation">
                    <ul class="left-bar-navigation__main">
                        <li><a href="{{route("app.settings")}}"><i class="fas fa-cog fa-lg"></i> <span style="padding-left: 11px">Settings</span></a> </li>
                    </ul>
                </nav>
            </div>
            @yield('content')
        </div>

        @if(trim(Auth::user()->password) == "")
            <div class="first-time-login">
                <h3>First time Login</h3>
                <p class="text-muted">
                    We have noticed this is your first time login in. Please insert a new password.
                </p>
                <hr class="separator">
                <div class="pt-2">
                    <p class="pl-3">User email address: <span style="color:green">kituyib@gmail.com</span></p>
                </div>
                <div class="p-0">
                    <form class="form-horizontal" method="POST" action="{{ route('app.new_password') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <div class="col-sm-12">
                                <input id="password" type="password" placeholder="New Password"
                                       class="form-control password" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password2') ? ' has-error' : '' }}">
                            <div class="col-sm-12">
                                <input id="password2" type="password" placeholder="Confirm Password"
                                       class="form-control password" name="password2" required>

                                @if ($errors->has('password2'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password2') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-12 text-center">
                                <button type="submit" class="btn btn-outline-success btn-new-password">
                                    Create New Password
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        @endif
    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js" ></script>
    <script src="https://code.jquery.com/ui/1.11.3/jquery-ui.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script src="{{ asset('plugins/toastr/toastr.min.js') }}"></script>
    @yield('scripts')
    <script>
        @if(Session::has('success'))
        toastr.success("{{ Session::get('success') }}");
        @endif

        @if(Session::has('info'))
        toastr.info("{{ Session::get('info') }}");
        @endif
    </script>
</div>
</body>
</html>

