@extends('layouts.app')


@section('content')
    <div class="app-body">
        <h4 class="app-body--title">
            Dashboard
        </h4>
        <hr class="separator">

        <div class="dashboard-notification">
            <div class="row">
                <div class="col-3">
                    <a href="" class="notification__link">
                    <div class="notification--value">
                        <h1 class="text-center">{{$tasks}}</h1>
                        <p class="text-muted text-center">Tasks</p>
                    </div>
                    </a>
                </div>
                <div class="col-3">
                    <a href="" class="notification__link">
                        <div class="notification--value">
                            <h1 class="text-center">00</h1>
                            <p class="text-muted text-center">Notifications</p>
                        </div>
                    </a>
                </div>
                <div class="col-3">
                    <a href="" class="notification__link">
                        <div class="notification--value">
                            <h1 class="text-center">{{$groups}}</h1>
                            <p class="text-muted text-center">Teams</p>
                        </div>
                    </a>
                </div>
                <div class="col-3">
                    <a href="" class="notification__link">
                        <div class="notification--value" style="border: none;">
                            <h1 class="text-center">00</h1>
                            <p class="text-muted text-center">Messages</p>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>

    @if($tasks < 1)
    <div class="app-body no-activity-recorded">
        <h2 class="no-activity-recorded text-center text-muted">You Have No Activity!</h2>
        <p class="text-info text-center">How do you feel about setting a up a Task, or chat with workmates?</p>
        <div class="row">
            <div class="col-6 text-right no-activity-action-button">
                <button class="btn btn-info">Chat</button>
            </div>
            <div class="col-6 text-left no-activity-action-button">
                <button class="btn btn-success">New Task</button>
            </div>

        </div>
    </div>
    @else
        @foreach($userCollection->individualTask as $task)
        <div class="app-body">
            <div class="task-name">
                <h4 class="app-body--title">
                    {{$task->task->title}}
                </h4>
            </div>
            <div class="task-body">
                <div class="participants">
                    <p class="text-muted">{{$task->task->user->name}}</p>
                    <hr>
                </div>
                <div class="participants-profile-pics">
                    {!! substr($task->task->description, 0, 200) !!}...
                </div>
            </div>
            <div class="progress-builder-percentage">
                <p class="text-muted">priority {{$task->task->priority}} % </p>
            </div>
            <div class="row">
                <div class="col-12">
                    <a href="{{ route('task.show', ['id' =>$task->task->id]) }}" class="btn btn-outline-success pull-right update_task_progress">Open Task</a>
                </div>
            </div>
            <div class="progress-builder" style="width: {{$task->task->priority}}%;"></div>
        </div>
        @endforeach
    @endif

@endsection

