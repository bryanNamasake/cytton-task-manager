@extends('layouts.app')

@section('content')
    <div class="app-body" style="overflow: hidden">
        <h4 class="app-body--title">
            Create New Task
        </h4>
        <hr class="separator">

        <form action="{{ route("task.store") }}" method="post" enctype="multipart/form-data" class="create-task-form">

            {{csrf_field()}}
            <div class="create-task-parts create-task-part-1">
                <div class="row justify-content-md-center">
                    <div class="col-md-8 col-lg-offset-8  mx-auto">
                        <div class="form-group">
                            <label for="name">Task Title</label>
                            <input type="text" id="name"
                                   name="name" class="form-control">
                        </div>
                    </div>

                    <div class="col-md-8 col-lg-offset-8  mx-auto">
                        <div class="form-group">
                            <label for="description">Task Description</label>
                            <textarea name="description" class="form-control"
                                      id="description" cols="30" rows="6">
                        </textarea>
                        </div>
                    </div>
                    {{--<div class="col-md-8 col-lg-offset-8 mx-auto">
                        <div class="form-group">
                            <label for="category">Select Category</label>
                            <select name="category_id" id="category" class="form-control">
                                <option class="" value="category">Category</option>
                            </select>
                        </div>
                    </div>--}}
                    <div class="col-md-8 col-lg-offset-8  mx-auto">
                        <div class="form-group">
                            <label for="priority_picker">Priority</label>
                            <input type="text" id="priority_picker" style="display: none"
                                   name="priority_picker" class="form-control">
                            <div id="slider-connect"></div>
                        </div>
                    </div>
                    <div class="col-md-8 col-lg-offset-8  mx-auto">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="start_date">Start Date</label>
                                    <input type="date" name="start_date" class="form-control"
                                           id="start_date"/>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="end_date">End Date</label>
                                    <input type="date" name="end_date" class="form-control"
                                           id="end_date"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8 col-lg-offset-8  mx-auto">
                        <div class="form-group">
                            <div class="text-center">
                                <button class="btn btn-success pull-right btn-page-one">
                                    Next
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="create-task-parts create-task-part-2" style="display: none">
                <div class="row justify-content-md-center">
                    <div class="col-md-8 col-lg-offset-8  mx-auto">
                        <hr class="separator">
                        <div class="form-group">
                            <label for="assign_category">How do you want to assign the task?</label>
                            <select name="assign_category" id="assign_category" class="form-control">
                                {{--<option class="" value="department" data-assign="by_department">By Department</option>--}}
                                <option class="" value="group" data-assign="by_group">By Group</option>
                                <option class="" value="individual" data-assign="by_individual">Choose Individuals</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-8 col-lg-offset-8  mx-auto assignment-by department">
                        <div class="get-by-department">
                            <div class="form-group">
                                <label for="departments">Department*s <span class="text-muted"> (Departments to send the work to)</span></label>

                                    <select class="departments js-states form-control getmdl-select getmdl-select__fullwidth departments"
                                            name="departments[]" id="departments"
                                            multiple="multiple" style="width: 100%">
                                        @foreach($departments as $department)
                                            <option class="" value="{{$department->id}}">{{$department->name}}</option>
                                        @endforeach
                                    </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8 col-lg-offset-8  mx-auto assignment-by group" style="display: none">
                        <div class="get-by-group">
                            <div class="form-group">
                                @if(count($groups) > 0)
                                    <label for="groups">Group*s <span class="text-muted"> (Choose Groups you have created.)</span></label>
                                    <select class="groups js-states form-control getmdl-select getmdl-select__fullwidth groups"
                                            name="groups[]" id="groups"
                                            multiple="multiple" style="width: 100%">
                                        @foreach($groups as $group)
                                            <option class="" value="{{$group->id}}">{{$group->name}}</option>
                                        @endforeach
                                    </select>
                                @else
                                    <p class="">You have no existing groups. Don't worry!, we got you covered. You can always create a new group.</p>
                                    <div class="col-6 text-left no-activity-action-button">
                                        <a href="{{route("group.create")}}" class="btn btn-success" style="margin-left: -15px;">Create a New Group</a>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8 col-lg-offset-8  mx-auto assignment-by individual" style="display: none">
                        <div class="get-by-individual">
                            <div class="form-group">
                                <input type="text" class="form-control" id="employee-details" autocomplete="false"
                                       placeholder="Enter employee name, email, employee ID">
                                <div class="employee-search-results">
                                    <div class="results-body">
                                        <div class="individual-result"></div>

                                    </div>
                                </div>
                                <div class="employees mt-3">
                                    <p class="text-muted" style="margin: 0; padding: 0; font-size: 14px">Added Group Members</p>
                                    <div class="selected-employees">

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8 col-lg-offset-8  mx-auto">
                        <hr class="separator">
                        <div class="form-group">
                            <label for="file_attachment">Attach Files</label>
                            <input type="file"  id="file_attachment" multiple="multiple"
                                   name="file_attachment[]" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-8 col-lg-offset-8  mx-auto">
                        <hr class="separator">
                        <div class="form-group">
                            <label for="make_public">Do you want this task to be public</label><br>
                            <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                <label class="btn btn-success active">
                                    <input type="radio" name="make_public" id="yes" autocomplete="off" value="yes" checked> YES
                                </label>
                                <label class="btn btn-danger">
                                    <input type="radio" name="make_public" id="no" autocomplete="off" value="no"> NO
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8 col-lg-offset-8  mx-auto">
                        <hr class="separator">
                        <div class="form-group">
                            <div class="text-center">
                                <button class="btn btn-success pull-right btn-create" type="submit">
                                    Create the Task
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection

@section('styles')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote-bs4.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/noUiSlider/11.0.3/nouislider.css" rel="stylesheet">
@stop

@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote-bs4.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/noUiSlider/11.0.3/nouislider.js"></script>
    <!-- select2-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/validate.js/0.12.0/validate.min.js"></script>
    <script>
        var connectSlider = $('#slider-connect').get(0);
        var priority_value = $('#priority_picker');

        $(document).ready(function () {
            $('#description').summernote({
                popover: {
                    image: [],
                    link: [],
                    air: []
                }
            });

            noUiSlider.create(connectSlider, {
                start: 40,
                connect: [true, false],
                range: {
                    'min': 0,
                    'max': 100
                }
            });
            connectSlider.noUiSlider.on('update', function( values, handle ) {
                priority_value.val(parseInt(values[0]));
            });

            $('.departments').select2();
            $('.groups').select2();


            $('.btn-page-one').on( "click", function(e) {
                //validate data
                var title = $(".title");
                e.preventDefault();
                $('.create-task-part-1').hide("slide", { direction: "left" }, 300);
                $(".create-task-part-2").show("slide", { direction: "right" }, 500);
            });

        });

        $('#assign_category').on('change', function (e) {
            e.preventDefault();

            var assign = $(this).val();
            $('.assignment-by').hide();

            var current_assignment = $('.assignment-by').data('assign');
            $('.assignment-by.' + assign).show();
        });

        var timeoutID = null;
        var search_div_results = $('.results-body');
        var selected_employees = $('.selected-employees');
        var employee_ids = [];
        var current_employee_id = {{Auth::user()->id}};
        var form = $('.create-task-form');


        function display_search(_data) {
            for(var i = 0; i<_data.length; i++){
                if(current_employee_id != _data[i].id) {
                    var result_data =
                        '<div class="individual-result">' +
                        '<div class="row">' +
                        '<div class="col-2">' +
                        '<div class="employee-picture">' +
                        '<img src="'+_data[i].avatar+'">' +
                        '</div>' +
                        '</div>' +
                        '<div class="col-5 pt-3">' +
                        '<p class="name"><strong>'+_data[i].name+'</strong></p>' +
                        '<p class="text-muted email">'+_data[i].email+'</p>' +
                        '<p class="text-muted employee_id">Employee ID: '+_data[i].employee_id+'</p>' +
                        '</div>' +
                        '<div class="col-5 pt-3 pr-3">' +
                        '<button class="btn btn-warning pull-right btn-add-group-employee mr-3" id="btn-add-group-employee'+i+'" data-employee_id="'+_data[i].id+'">' +
                        'Add to Group' +
                        '</button>' +
                        '</div>' +
                        '</div>' +
                        '</div>';
                    search_div_results.append(result_data);
                    $('#btn-add-group-employee'+i).data('data_employee',_data[i]);
                }
            }
        }

        function findMember(searchStr) {
            if(searchStr.trim() !== ""){
                $.ajax('{{route('employee.search')}}', {
                    method: "GET",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: {"search_value" : searchStr},
                    beforeSend: function() {
                    },
                    success: function (data) {
                        console.log(data);
                        if(data) {
                            var _data = data;
                            console.log(_data);
                            if (_data.length > 0) {
                                search_div_results.html('');
                                search_div_results.show();
                                display_search(_data);
                            }else{
                                search_div_results.html('NO EMPLOYEES WITH THAT SEARCH TERM FOUND');
                            }
                        }else{
                            search_div_results.html('NO EMPLOYEES WITH THAT SEARCH TERM FOUND');
                        }
                    }
                });
            }
        }

        $('#employee-details').keyup(function(e) {
            clearTimeout(timeoutID);
            timeoutID = setTimeout(findMember.bind(undefined, e.target.value), 1);
        });

        search_div_results.on('click', '.btn-add-group-employee', function (e) {
            e.preventDefault();
            search_div_results.hide();
            var employee_id = $(this).data('employee_id');

            if(employee_ids.indexOf(employee_id) > -1){
                alert('user already added');
            }else{
                var selected_employee = $(this).data('data_employee');
                var result_data =
                    '<div class="individual-result" id="select_employee"'+selected_employee.employee_id+'>' +
                    '<div class="row">' +
                    '<div class="col-2">' +
                    '<div class="employee-picture">' +
                    '<img src="'+selected_employee.avatar+'">' +
                    '</div>' +
                    '</div>' +
                    '<div class="col-5 pt-3">' +
                    '<p class="name"><strong>'+selected_employee.name+'</strong></p>' +
                    '<p class="text-muted email">'+selected_employee.email+'</p>' +
                    '<p class="text-muted employee_id">Employee ID: '+selected_employee.employee_id+'</p>' +
                    '</div>' +
                    '<div class="col-5 pt-3 pr-3">' +
                    '<button class="btn btn-warning pull-right btn-remove-group-employee mr-3" id="btn-remove-group-employee'+employee_id+'" data-employee_id="'+selected_employee.id+'">' +
                    'Remove From Group' +
                    '</button>' +
                    '</div>' +
                    '</div>' +
                    '</div>';
                selected_employees.append(result_data);
                employee_ids.push(employee_id);
            }
        });

        selected_employees.on('click', '.btn-remove-group-employee', function (e) {
            e.preventDefault();
            var id_to_remove = $(this).data('employee_id');

            console.log(id_to_remove);
            var index = employee_ids.indexOf(id_to_remove);


            if (index > -1) {
                employee_ids.splice(index, 1);
                $(this).animate({
                    "opacity" : "0"
                },{
                    "complete" : function() {
                        $(this).closest('.individual-result').remove();
                    }
                });
                toastr.success("Employee removed from group");
            }
        });

        form.submit( function(eventObj) {
            for(var id = 0; id < employee_ids.length; id++){

                if(employee_ids.length > 0){
                    $('<input />').attr('type', 'hidden')
                        .attr('name', "user_id[]")
                        .attr('value', employee_ids[id])
                        .appendTo(form);
                }
            }

            console.log(form.serialize());
            return true;
        });

    </script>
@stop

