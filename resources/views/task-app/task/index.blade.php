@extends('layouts.app')


@section('content')
    <div class="app-body" style="overflow: hidden">
        <h4 class="app-body--title">
            All Task
        </h4>
        <hr class="separator">
    </div>


    @if(count($individualTask))
        @foreach($individualTask as $task)
            <div class="app-body" style="overflow: hidden">
                <div class="row task">
                    <div class="col-2">
                        <p class="text-muted task-title">
                            Task Title
                        </p>
                        <p>
                            <strong>
                                {{$task->task->title}}
                            </strong>
                        </p>
                    </div>
                    <div class="col-2">
                        <p class="text-muted task-title">
                            Task Creator
                        </p>
                        <p>
                            <strong>
                                {{$task->task->user->name}}
                            </strong>
                        </p>
                    </div>
                    <div class="col-2">
                        <p class="text-muted task-title">
                            Date Created
                        </p>
                        <p>
                            <strong>
                                {{ date("Y-m-d", strtotime($task->task->start_date)) }}
                            </strong>
                        </p>
                    </div>
                    <div class="col-2">
                        <p class="text-muted task-title">
                            Priority
                        </p>
                        <p>
                            <strong>
                                {{$task->task->priority}} %
                            </strong>
                        </p>
                    </div>
                    <div class="col-2">
                        <p class="text-muted task-title">
                            Assigned To
                        </p>
                        <p>
                            <strong>
                                {{$task->task->assigned_by}}
                            </strong>
                        </p>
                    </div>
                    <div class="col-2 text-center">
                        <a href="{{ route('task.show', ['id' =>$task->task->id]) }}"
                           class="btn btn-outline-success view-task mt-3">
                            View Task
                        </a>
                    </div>
                </div>
            </div>
        @endforeach
    @endif


    @if(count($groupTask))
        @foreach($groupTask as $task)
            @foreach($task->groupTask as $_task)
                <div class="app-body" style="overflow: hidden; background: #eef0f0">
                <div class="row task">
                    <div class="col-2">
                        <p class="text-muted task-title">
                            Task Title
                        </p>
                        <p>
                            <strong>
                                {{$_task->task->title}}
                            </strong>
                        </p>
                    </div>
                    <div class="col-2">
                        <p class="text-muted task-title">
                            Task Creator
                        </p>
                        <p>
                            <strong>
                                {{$_task->task->user->name}}
                            </strong>
                        </p>
                    </div>
                    <div class="col-2">
                        <p class="text-muted task-title">
                            Date Created
                        </p>
                        <p>
                            <strong>
                                {{ date("Y-m-d", strtotime($_task->task->start_date)) }}
                            </strong>
                        </p>
                    </div>
                    <div class="col-2">
                        <p class="text-muted task-title">
                            Priority
                        </p>
                        <p>
                            <strong>
                                {{$_task->task->priority}} %
                            </strong>
                        </p>
                    </div>
                    <div class="col-2">
                        <p class="text-muted task-title">
                            Assigned To
                        </p>
                        <p>
                            <strong>
                                {{$_task->task->assigned_by}}
                            </strong>
                        </p>
                    </div>
                    <div class="col-2 text-center">
                        <a href="{{ route('task.show', ['id' =>$_task->task->id]) }}"
                           class="btn btn-outline-success view-task mt-3">
                            View Task
                        </a>
                    </div>
                </div>
            </div>
            @endforeach
        @endforeach
    @endif

@endsection