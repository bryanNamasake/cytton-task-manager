@extends('layouts.app')


@section('content')
    <div class="app-body" style="overflow: hidden">
        <h3 class="app-body--title">
            {{ $task_details->title }}
        </h3>
        <div class="row task--metadata">
            <div class="col-2">
                <div style="float: left">
                    <img src="{{ $task_details->user->avatar }}" class="profile-picture-small">
                </div>
                <div style="float: left" class="pt-2">
                    <p class="text-muted">Created By</p>
                    <p style="margin-top: -10px;">{{ $task_details->user->name }}</p>
                </div>
            </div>
            <div class="col-2 pt-2">
                <i class="far fa-clock fa-lg pt-2 pr-1" style="float: left"></i>
                <div style="float: left">
                    <p class="text-muted">Due Date</p>
                    <p style="margin-top: -10px;">{{ date("Y-m-d", strtotime( $task_details->end_date)) }}</p>
                </div>
            </div>
            <div class="col-2 pt-2">
                <p class="text-muted">Status</p>
                <p style="margin-top: -10px;">
                    @if($task_details->completed == "no")
                        <span class="text-success">In Progress</span>
                    @else
                        <span class="text-warning">Completed</span>
                    @endif
                </p>
            </div>
            <div class="col-6 pt-2" style="float: right">
                <a href="#" class="btn btn-outline-info progress-update--btn" data-toggle="modal" data-target="#progress-modal">Update Your Progress</a>
            </div>
        </div>

        <p class="text-muted pull-right-view">Priority - {{$task_details->priority}}%</p>
        <div class="priority-holder">
            <div class="priority" style="width: {{$task_details->priority}}%">

            </div>
        </div>
        <div class="task--body">
            <hr class="separator">
            <p>
                {!! $task_details->description !!}
            </p>
        </div>

        <div class="task-attachments">
            <hr class="separator">
            <p class="text-muted pull-right-view">Attachments</p>
            @if($task_details->attachment == 'yes')
                @if(count($task_details->tackAttachment))
                    @foreach($task_details->tackAttachment as $attachment)
                        <a href="{{@asset($attachment->attachment_url)}}" download
                           class="badge badge-success attachment-badge">{{substr($attachment->attachment_url, 27, -1)}}</a>
                    @endforeach
                @endif
            @else
                <p>No Attachment</p>
            @endif
        </div>
        <div class="task-participants">
            <hr class="separator">
            <p class="text-muted pull-right-view">Participants</p>
            {{--{{dd($task_details->individualTask->user)}}--}}
            @if(count($task_details->individualTask) > 0)
                @foreach($task_details->individualTask as $user)
                    <div class="row pt-2 pb-2" style="border-bottom: 1px solid #e4e4e4">
                        <div class="col-2">
                            <div style="float: left">
                                <img src="{{ $user->user->avatar }}" class="profile-picture-small">
                            </div>
                            <div style="float: left" class="pt-4">
                                <p style="margin-top: -10px;">{{$user->user->name}}</p>
                            </div>
                        </div>
                        <div class="col-3 pt-4">
                            <p style="margin-top: -10px;">{{$user->user->email}}</p>
                        </div>
                        <div class="col-2 pt-4">
                            <p style="margin-top: -10px;">Employee ID: {{$user->user->employee_id}}</p>
                        </div>
                        <div class="col-3 pt-3">
                            @if(count($user->user->employeeProgress))
                                <p style="margin-top: -10px;">progress: {{$user->user->employeeProgress->progress}}
                                    %</p>
                            @else
                                <p>No Progress Recorded</p>
                            @endif
                        </div>
                        <div class="col-2 pt-2">
                            @if($user->user->id != Auth::id())
                                <a href="#" class="btn btn-outline-warning progress-update--btn">Message</a>

                            @endif
                        </div>
                    </div>
                @endforeach
            @endif

            @if(count($task_details->groupTask) > 0)
                @foreach($task_details->groupTask as $_user)
                    @foreach($_user->group->employeeGroup as $user)
                    <div class="row pt-2 pb-2" style="border-bottom: 1px solid #e4e4e4">
                        <div class="col-2">
                            <div style="float: left">
                                <img src="{{ $user->user->avatar }}" class="profile-picture-small">
                            </div>
                            <div style="float: left" class="pt-4">
                                <p style="margin-top: -10px;">{{$user->user->name}}</p>
                            </div>
                        </div>
                        <div class="col-3 pt-4">
                            <p style="margin-top: -10px;">{{$user->user->email}}</p>
                        </div>
                        <div class="col-2 pt-4">
                            <p style="margin-top: -10px;">Employee ID: {{$user->user->employee_id}}</p>
                        </div>
                        <div class="col-3 pt-3">
                            @if(count($user->employeeProgress))
                                <p style="margin-top: -10px;">progress: {{$user->user->employeeProgress->progress}}
                                    %</p>
                            @else
                                <p>No Progress Recorded</p>
                            @endif
                        </div>
                        <div class="col-2 pt-2">
                            @if($user->user->id != Auth::id())
                                <a href="#" class="btn btn-outline-warning progress-update--btn">Message</a>

                            @endif
                        </div>
                    </div>
                        @endforeach
                @endforeach
            @endif
        </div>
    </div>

    <div class="modal fade" id="progress-modal" tabindex="-1" role="dialog" aria-labelledby="progressModalLabel" aria-hidden="true" style="display: none;">
        <div class="progressmodal-container">
            <div class="col-md-10 col-lg-offset-10  mx-auto">
                <h3 class="app-body--title">Update your progress</h3>
                <hr class="separator">
            </div>
            <form action="{{route('task.progress')}}" method="post">
                {{csrf_field()}}
                <div class="col-md-10 col-lg-offset-10  mx-auto">
                    <div class="form-group">
                        <label for="progress_picker">Progress</label>
                        <input type="text" id="progress_picker" style="display: none"
                               name="progress_picker" class="form-control">
                        <div id="slider-connect"></div>
                    </div>
                </div>
                <div class="col-md-10 col-lg-offset-10 mx-auto mt-4" style="margin-top: 15px">
                    <div class="form-group">
                        <label for="description">Progress Message</label>
                        <textarea name="description" class="form-control"
                                  id="description" cols="30" rows="10"></textarea>
                    </div>
                </div>
                <div class="col-md-10 col-lg-offset-10 mx-auto mt-4"  style="margin-top: 15px">
                    <div class="form-group">
                        <button  class="btn btn-success progress-submit " style="float: right" type="submit">Update Progress</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection


@section('styles')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote-bs4.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/noUiSlider/11.0.3/nouislider.css" rel="stylesheet">
@stop

@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote-bs4.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/noUiSlider/11.0.3/nouislider.js"></script>

    <script>
        var connectSlider = $('#slider-connect').get(0);
        var priority_value = $('#progress_picker');

        $(document).ready(function () {
            $('#description').summernote({
                popover: {
                    image: [],
                    link: [],
                    air: []
                }
            });

            noUiSlider.create(connectSlider, {
                start: 40,
                connect: [true, false],
                range: {
                    'min': 0,
                    'max': 100
                }
            });
            connectSlider.noUiSlider.on('update', function (values, handle) {
                priority_value.val(parseInt(values[0]));
            });

            /*$('.progress-update--btn').on('click', function (event) {

                return true;
            });*/
        });
    </script>
@stop