@extends('layouts.app')


@section('content')
    <div class="app-body" style="overflow: hidden">
        <h4 class="app-body--title">
            Create New Group/team
        </h4>
        <hr class="separator">

        <form action="{{route('group.store')}}" method="post" enctype="multipart/form-data" class="create-group-form">
            {{csrf_field()}}
            <div class="row justify-content-md-center">
                <div class="col-md-8 col-lg-offset-8  mx-auto">
                    <div class="form-group">
                        <label for="name">Group Name</label>
                        <input type="text" id="name"
                               name="name" class="form-control">
                    </div>
                </div>
                <div class="col-md-8 col-lg-offset-8  mx-auto">
                    <label for="employee-details">Add Group Participants</label>
                    <div class="get-by-individual">
                        <div class="form-group">
                            <input type="text" class="form-control" id="employee-details" autocomplete="false"
                                   placeholder="Enter employee name, email, employee ID">
                            <div class="employee-search-results">
                                <div class="results-body">
                                    <div class="individual-result"></div>

                                </div>
                            </div>
                            <div class="employees mt-3">
                                <p class="text-muted" style="margin: 0; padding: 0; font-size: 14px">Added Group Members</p>
                                <div class="selected-employees">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-8 col-lg-offset-8  mx-auto">
                    <hr class="separator">
                    <div class="form-group">
                        <div class="text-center">
                            <button class="btn btn-success pull-right btn-create btn-create-group">
                                Create the Group
                            </button>
                        </div>
                    </div>
                </div>
            </div>
       </form>
    </div>

@endsection

@section('scripts')
    <!-- select2-->
    <script src="//cdnjs.cloudflare.com/ajax/libs/validate.js/0.12.0/validate.min.js"></script>
    <script>
        var timeoutID = null;
        var search_div_results = $('.results-body');
        var selected_employees = $('.selected-employees');
        var employee_ids = [];
        var current_employee_id = {{Auth::user()->id}};
        var form = $('.create-group-form');


        function display_search(_data) {
            for(var i = 0; i<_data.length; i++){
                if(current_employee_id != _data[i].id) {
                    var result_data =
                        '<div class="individual-result">' +
                        '<div class="row">' +
                        '<div class="col-2">' +
                        '<div class="employee-picture">' +
                        '<img src="'+_data[i].avatar+'">' +
                        '</div>' +
                        '</div>' +
                        '<div class="col-5 pt-3">' +
                        '<p class="name"><strong>'+_data[i].name+'</strong></p>' +
                        '<p class="text-muted email">'+_data[i].email+'</p>' +
                        '<p class="text-muted employee_id">Employee ID: '+_data[i].employee_id+'</p>' +
                        '</div>' +
                        '<div class="col-5 pt-3 pr-3">' +
                        '<button class="btn btn-warning pull-right btn-add-group-employee mr-3" id="btn-add-group-employee'+i+'" data-employee_id="'+_data[i].id+'">' +
                        'Add to Group' +
                        '</button>' +
                        '</div>' +
                        '</div>' +
                        '</div>';
                    search_div_results.append(result_data);
                    $('#btn-add-group-employee'+i).data('data_employee',_data[i]);
                }
            }
        }

        function findMember(searchStr) {
            if(searchStr.trim() !== ""){
                $.ajax('{{route('employee.search')}}', {
                    method: "GET",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: {"search_value" : searchStr},
                    beforeSend: function() {
                    },
                    success: function (data) {
                        console.log(data);
                        if(data) {
                            var _data = data;
                            console.log(_data);
                            if (_data.length > 0) {
                                search_div_results.html('');
                                search_div_results.show();
                                display_search(_data);
                            }else{
                                search_div_results.html('NO EMPLOYEES WITH THAT SEARCH TERM FOUND');
                            }
                        }else{
                            search_div_results.html('NO EMPLOYEES WITH THAT SEARCH TERM FOUND');
                        }
                    }
                });
            }
        }

        $('#employee-details').keyup(function(e) {
            clearTimeout(timeoutID);
            timeoutID = setTimeout(findMember.bind(undefined, e.target.value), 1);
        });

        search_div_results.on('click', '.btn-add-group-employee', function (e) {
            e.preventDefault();
            search_div_results.hide();
            var employee_id = $(this).data('employee_id');

            if(employee_ids.indexOf(employee_id) > -1){
                alert('user already added');
            }else{
                var selected_employee = $(this).data('data_employee');
                var result_data =
                    '<div class="individual-result" id="select_employee"'+selected_employee.employee_id+'>' +
                    '<div class="row">' +
                    '<div class="col-2">' +
                    '<div class="employee-picture">' +
                    '<img src="'+selected_employee.avatar+'">' +
                    '</div>' +
                    '</div>' +
                    '<div class="col-5 pt-3">' +
                    '<p class="name"><strong>'+selected_employee.name+'</strong></p>' +
                    '<p class="text-muted email">'+selected_employee.email+'</p>' +
                    '<p class="text-muted employee_id">Employee ID: '+selected_employee.employee_id+'</p>' +
                    '</div>' +
                    '<div class="col-5 pt-3 pr-3">' +
                    '<button class="btn btn-warning pull-right btn-remove-group-employee mr-3" id="btn-remove-group-employee'+employee_id+'" data-employee_id="'+selected_employee.id+'">' +
                    'Remove From Group' +
                    '</button>' +
                    '</div>' +
                    '</div>' +
                    '</div>';
                selected_employees.append(result_data);
                employee_ids.push(employee_id);
            }
        });

        selected_employees.on('click', '.btn-remove-group-employee', function (e) {
            e.preventDefault();
            var id_to_remove = $(this).data('employee_id');

            console.log(id_to_remove);
            var index = employee_ids.indexOf(id_to_remove);


            if (index > -1) {
                employee_ids.splice(index, 1);
                $(this).animate({
                    "opacity" : "0"
                },{
                    "complete" : function() {
                        $(this).closest('.individual-result').remove();
                    }
                });
                toastr.success("Employee removed from group");
            }
        });

        form.submit( function(eventObj) {
            for(var id = 0; id < employee_ids.length; id++){
                $('<input />').attr('type', 'hidden')
                    .attr('name', "user_id[]")
                    .attr('value', employee_ids[id])
                    .appendTo(form);
            }
            return true;
        });

    </script>
@stop

