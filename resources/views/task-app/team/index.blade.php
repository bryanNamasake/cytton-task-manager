@extends('layouts.app')


@section('content')
    <div class="app-body" style="overflow: hidden">
        <h4 class="app-body--title">
            All Your Groups
        </h4>
        <hr class="separator">
        <a href="{{ route('group.create')}}"
           class="btn btn-outline-info view-task mt-3">
            Create New Group
        </a>
    </div>


    @if(count($employeeGroups))
        @foreach($employeeGroups as $employeeGroup)
            <div class="app-body" style="overflow: hidden">
                <div class="row task">
                    <div class="col-2">
                        <p class="text-muted task-title">
                            Task Title
                        </p>
                        <p>
                            <strong>
                                {{$employeeGroup->group->name}}
                            </strong>
                        </p>
                    </div>
                    <div class="col-2">
                        <p class="text-muted task-title">
                            Members Count
                        </p>
                        <p>
                            <strong>
                                {{count($employeeGroup->group->employeeGroup)}}
                            </strong>
                        </p>
                    </div>
                    <div class="col-2">
                        <p class="text-muted task-title">
                            Date Created
                        </p>
                        <p>
                            <strong>
                                {{ date("Y-m-d", strtotime($employeeGroup->group->created_at)) }}
                            </strong>
                        </p>
                    </div>
                    <div class="col-2">
                        <p class="text-muted task-title">
                            Creator
                        </p>
                        <p>
                            <strong>
                                {{$employeeGroup->group->user->name}}
                            </strong>
                        </p>
                    </div>
                    <div class="col-4 text-center">
                        <a href="{{ route('group.show', ['id' =>$employeeGroup->group->id]) }}"
                           class="btn btn-outline-success view-task mt-3">
                            View Group
                        </a>
                    </div>
                </div>
            </div>
        @endforeach
    @endif

@endsection