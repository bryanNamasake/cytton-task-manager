@extends('layouts.app')


@section('content')
    <div class="app-body" style="overflow: hidden">
        <h3 class="app-body--title">
            {{ $group_details->name }}
        </h3>
        <div class="row task--metadata">
            <div class="col-2">
                <div style="float: left">
                    <img src="{{ $group_details->user->avatar }}" class="profile-picture-small">
                </div>
                <div style="float: left" class="pt-2">
                    <p class="text-muted">Created By</p>
                    <p style="margin-top: -10px;">{{ $group_details->user->name }}</p>
                </div>
            </div>
            <div class="col-2 pt-2">
                <i class="far fa-clock fa-lg pt-2 pr-1" style="float: left"></i>
                <div style="float: left">
                    <p class="text-muted">Created Date</p>
                    <p style="margin-top: -10px;">{{ date("Y-m-d", strtotime( $group_details->created_at)) }}</p>
                </div>
            </div>
            <div class="col-8"></div>
            <hr class="separator">
        </div>
        <div class="task-participants">
            <hr class="separator">
            <p class="text-muted pull-right-view">Participants</p>
            {{--{{dd($task_details->individualTask->user)}}--}}
            @if(count($group_details->employeeGroup) > 0)
                @foreach($group_details->employeeGroup as $user)
                    <div class="row pt-2 pb-2" style="border-bottom: 1px solid #e4e4e4">
                        <div class="col-2">
                            <div style="float: left">
                                <img src="{{ $user->user->avatar }}" class="profile-picture-small">
                            </div>
                            <div style="float: left" class="pt-4">
                                <p style="margin-top: -10px;">{{$user->user->name}}</p>
                            </div>
                        </div>
                        <div class="col-3 pt-4">
                            <p style="margin-top: -10px;">{{$user->user->email}}</p>
                        </div>
                        <div class="col-2 pt-4">
                            <p style="margin-top: -10px;">Employee ID: {{$user->user->employee_id}}</p>
                        </div>
                        <div class="col-3 pt-3">
                            @if(count($user->user->employeeProgress))
                                <p style="margin-top: -10px;">progress: {{$user->user->employeeProgress->progress}}
                                    %</p>
                            @else
                                <p>No Progress Recorded</p>
                            @endif
                        </div>
                        <div class="col-2 pt-2">
                            @if($user->user->id != Auth::id())
                                <a href="#" class="btn btn-outline-warning progress-update--btn">Message</a>

                            @endif
                        </div>
                    </div>
                @endforeach
            @endif

        </div>

        <div class="task-participants">
            <p class="text-muted pull-right-view">Tasks</p>
            @if(count($group_details->groupTask) > 0)
                    @foreach($group_details->groupTask as $_task)
                    <div class="row task">
                        <div class="col-2">
                            <p class="text-muted task-title">
                                Task Title
                            </p>
                            <p>
                                <strong>
                                    {{$_task->task->title}}
                                </strong>
                            </p>
                        </div>
                        <div class="col-2">
                            <p class="text-muted task-title">
                                Task Creator
                            </p>
                            <p>
                                <strong>
                                    {{$_task->task->user->name}}
                                </strong>
                            </p>
                        </div>
                        <div class="col-2">
                            <p class="text-muted task-title">
                                Date Created
                            </p>
                            <p>
                                <strong>
                                    {{ date("Y-m-d", strtotime($_task->task->start_date)) }}
                                </strong>
                            </p>
                        </div>
                        <div class="col-2">
                            <p class="text-muted task-title">
                                Priority
                            </p>
                            <p>
                                <strong>
                                    {{$_task->task->priority}} %
                                </strong>
                            </p>
                        </div>
                        <div class="col-2">
                            <p class="text-muted task-title">
                                Assigned To
                            </p>
                            <p>
                                <strong>
                                    {{$_task->task->assigned_by}}
                                </strong>
                            </p>
                        </div>
                        <div class="col-2 text-center">
                            <a href="{{ route('task.show', ['id' =>$_task->task->id]) }}"
                               class="btn btn-outline-success view-task mt-3">
                                View Task
                            </a>
                        </div>
                    </div>
                @endforeach

            @else
                <p>NO TASKS</p>
            @endif

        </div>
    </div>
@endsection