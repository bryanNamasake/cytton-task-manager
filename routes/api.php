<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix'=>'v1'], function (){

    // list departments
    Route::get('department', 'DepartmentController@index');

    // list single department
    Route::get('department/{id}', 'DepartmentController@show');

    // create new department
    Route::post('departments', 'DepartmentController@store');

    // create new department
    Route::put('department', 'DepartmentController@store');

    // create new department
    Route::delete('department', 'DepartmentController@destroy');



    Route::get('employee', 'EmployeeController@index');

    Route::get('employee/{id}', 'EmployeeController@show');

    Route::post('employee/verify', 'EmployeeController@verifyEmployee');

    Route::post('employee/get_department', 'EmployeeController@get_department');

/*
    Route::post('user/first_time/password', 'UsersController@new_password');*/
});

