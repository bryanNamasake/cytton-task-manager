<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',[
    'uses' => 'HomeController@index',
    'as' => 'home'
]);

Route::get('{provider}/auth',[
    'uses' => 'SocialsController@auth',
    'as' => 'social.auth'
]);

Route::get('{provider}/redirect',[
    'uses' => 'SocialsController@auth_callback',
    'as' => 'social.callback'
]);

Route::group(['prefix'=>'app',  'middleware' => ['admin.middleware']], function (){
    Route::get('/home',[
        'uses' => 'ApplicationController@index',
        'as' => 'app.home'
    ]);

    Route::post('/first-time/new_password', [
        'uses' => 'UsersController@new_password',
        'as' => 'app.new_password'
    ]);

    Route::get('/employee/search',[
        'uses' => 'EmployeeController@search',
        'as' => 'employee.search'
    ]);

    /*******************Notifications**********************/
    Route::get('/notification',[
        'uses' => 'NotificationsController@index',
        'as' => 'app.notifications'
    ]);


    /*******************TASKS**********************/
    Route::get('/task',[
        'uses' => 'TasksController@index',
        'as' => 'app.tasks'
    ]);

    Route::get('/task/create',[
        'uses' => 'TasksController@create',
        'as' => 'task.create'
    ]);

    Route::get('/task/{id}',[
        'uses' => 'TasksController@show',
        'as' => 'task.show'
    ]);

    Route::POST('/task/store',[
        'uses' => 'TasksController@store',
        'as' => 'task.store'
    ]);

    Route::POST('/task/progress',[
        'uses' => 'TasksController@progress',
        'as' => 'task.progress'
    ]);



    /*******************GROUPS**********************/
    Route::get('/group',[
        'uses' => 'GroupsController@index',
        'as' => 'app.groups'
    ]);

    Route::get('/group/create',[
        'uses' => 'GroupsController@create',
        'as' => 'group.create'
    ]);

    Route::get('/group/{id}',[
        'uses' => 'GroupsController@show',
        'as' => 'group.show'
    ]);

    Route::POST('/group/store',[
        'uses' => 'GroupsController@store',
        'as' => 'group.store'
    ]);

    /*******************Messasges**********************/
    Route::get('/messages',[
        'uses' => 'MessagesController@index',
        'as' => 'app.messages'
    ]);

    /*******************Messasges**********************/
    Route::get('/settings',[
        'uses' => 'SettingsController@index',
        'as' => 'app.settings'
    ]);
});
Auth::routes();

